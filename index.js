console.log("hello world");

function printInput(){
	//let nickname = prompt("Enter your nickname:");
	console.log("Hi, "+nickname);
}

//printInput();

// Consider this function
	// Parameter passing

	function printName(name){
		console.log("My name is " +name);
	}

	//argument
	printName("Danbell");
	printName("Db");
	printName();

// [SECTION] Parameters and Arguments
	
	// Parameter
		// "name" is parameter
		//  acts as a named variable/container taht only exists inside a function.
		// it is used to store informnation that is provided to a function when it/is called/invoked.

	// Arguments 
		//"Tolits" is the value/data passed directly into the function.
		// Values passed when invoking a function are called arguments.
		// Those arguments are then stored as the parameters within a function. 

	function checkDivisibilityBy8(num){
			let remainder = num % 8;
			console.log("The remainder of " + num + " divided by 8 is: " + remainder);
			let isDivisibleBy8 = remainder === 0;
			console.log("Is " + num + " divisible by 8?");
			console.log(isDivisibleBy8);

			printName("Danbell");
		}

		checkDivisibilityBy8(10);
		checkDivisibilityBy8(32);

		//  A variable as the argument.

		let num1 = 64;
		checkDivisibilityBy8(num1);

// [SECTION] Functions as Arguments
	// Function Parameters can also accepts other function as arguments.
	// Some complex function use other functions as arguments to perform more complicated results.
	// This is futher seen in Arrays 

		function argumentFunc(){
			console.log("This function was passed as an argument before the message was printed.")
		}

		function invokeFunc(argFunc){
			 console.log(argFunc);
			argFunc();
		}

		// Adding and removing the parenthesis " () " impacts the output of javascript.
			//function is used with parenthesis it denotes incoke/call
		// function is used withoutr a parenthesis is normally associated with using a function as an argument to another function
		invokeFunc(argumentFunc);

// [SECTION] Multipe Parameters
	
	// Multiple "arguments" will corresp0ond to the number of "parameters" declared in a function in succeeding order.

		function createFullName(firstName, middleName, lastName){
			console.log(firstName+" "+middleName+" "+lastName);
		}

		createFullName("Juan", "Enye", "Dela Cruz");
		createFullName("Juan", "Dela Cruz");
		createFullName("Juan", "Enye", "Dela Cruz", "Jr");


		// UIsing variables as arguments
		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName(firstName, middleName, lastName);
		// Note: the order of te arguments is the same to the order of the "parameters". The first arguments will be stored in the first paramenters, second parameters will be store in the second parameter and so on.

// [SECTION] Return Statement
	// allows us to output a value from a function to be passed the line/ code block of the code that invokes/called the function.
	// we can futher use/manipulate in or program instead of only printing/displaying it on the console.

	function returnFullName(firstName, middleName, lastName){
		// expected return Jeffrey Smith Doe
		return firstName+" "+middleName+" "+lastName;

		// return indicates the end of function execution
		// it will ignore any codes after return statement.
		console.log(firstName+" "+middleName+" "+lastName);
	}

	let completeName = returnFullName("Jefrey", "Smith", "Doe");
	console.log(completeName);

	// You can also create a variabe inside the function to contain the result and result the variable.

	function returnAddress(city, country){
		let fullAddress = city+ ", "+ country;
		return fullAddress;
	}

	let myAddress = returnAddress("Cebu City", "Cebu");
	console.log(myAddress);
	console.log(returnAddress("Cebu City", "Cebu"));

	// when a function only has a console.log() to display the result it will return undefined instead.
	
	function printPlayerInfo(usernname, level, job){
		console.log("Username: " + usernname);
		console.log("Level: "+level);
		console.log("Job: "+job);
	}

	let user = printPlayerInfo("Knight_white", 95, "Paladin");
	// You cannot save any value from printPlayerInfo() because it does not return anyrthing
	console.log(user);
	